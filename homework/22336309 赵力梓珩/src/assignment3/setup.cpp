#include "asm_utils.h"

#include "interrupt.h"

#include "stdio.h"

#include "program.h"

#include "thread.h"

#include "sync.h"

#include "memory.h"



// 屏幕IO处理器

STDIO stdio;

// 中断管理器

InterruptManager interruptManager;

// 程序管理器

ProgramManager programManager;

// 内存管理器

MemoryManager memoryManager;



// 引用字符串

int reference_string[20] = {7,0,1,2,0,3,0,4,2,3,0,3,2,1,2,0,1,7,0,1};



void first_thread(void *arg)

{

    int hit_count = 0;      // 命中次数

    int miss_count = 0;     // 缺页错误次数

    // 循环引用字符串的页面

    for (int i = 0; i < 20; i++) {

        int virtualAddress = 0xc0100000 + (reference_string[i] << 12);  // 虚拟地址

        // 计算虚拟地址对应的页目录项和页表项

        int *pde = (int *)memoryManager.toPDE(virtualAddress);

        int *pte = (int *)memoryManager.toPTE(virtualAddress);

        // 如果页目录项或页表项不存在，则发生缺页错误，需进行页面置换

        if(!(*pde & 0x00000001) || !(*pte & 0x00000001)) {

            memoryManager.page_replacement(AddressPoolType:: KERNEL,virtualAddress);

            miss_count++;

        }

        // 否则命中

        else {

            hit_count++;

        }

        // 输出当前内存中的虚拟页号

        if (miss_count == 1) {

      	    printf("\n");

            printf("Now Page %x is in memory. \n", (memoryManager.FIFO_queue[2] & 0x0000f000)>>12);

        }

        else if(miss_count == 2) {

	    printf("\n");

            printf("Now Page %x and Page %x are in memory. \n", (memoryManager.FIFO_queue[1] & 0x0000f000)>>12, (memoryManager.FIFO_queue[2] & 0x0000f000)>>12);

        }

        else if(miss_count >= 3){

	    printf("\n");

            printf("Now Page %x, Page %x and Page %x are in memory. \n", (memoryManager.FIFO_queue[0] & 0x0000f000)>>12, (memoryManager.FIFO_queue[1] & 0x0000f000)>>12, (memoryManager.FIFO_queue[2] & 0x0000f000)>>12);

        }

    }

      

    asm_halt();

}



extern "C" void setup_kernel()

{



    // 中断管理器

    interruptManager.initialize();

    interruptManager.enableTimeInterrupt();

    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);



    // 输出管理器

    stdio.initialize();



    // 进程/线程管理器

    programManager.initialize();



    // 内存管理器

    memoryManager.openPageMechanism();

    memoryManager.initialize();



    // 创建第一个线程

    int pid = programManager.executeThread(first_thread, nullptr, "first thread", 1);

    if (pid == -1)

    {

        printf("can not execute thread\n");

        asm_halt();

    }



    ListItem *item = programManager.readyPrograms.front();

    PCB *firstThread = ListItem2PCB(item, tagInGeneralList);

    firstThread->status = RUNNING;

    programManager.readyPrograms.pop_front();

    programManager.running = firstThread;

    asm_switch_thread(0, firstThread);



    asm_halt();

}